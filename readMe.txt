Psalm Joy Tobias
Full-Stack Developer Intern Applicant

Framework used
	Node.js
	Next.js
	React.js
	MongoDB

NPM installed
	npx create-next-app
	npm install express mongoose nodemon
	npm install jsonwebtoken
	npm install cors
	npm install bcrypt
	npm install dotenv
	npm install sweetalert2