const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'Insert First Name']
    },
    lastName: {
        type: String,
        required: [true, 'Insert Last Name']
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile Number is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    password: {
        type: String,
        // required: [true, 'Password is required']
    },
    totalMoney: {
        type: Number,
        default: 0
    },
    loginType: {
        type: String,
        required: [true, 'Login type is required']
    },
    isAdmin: {
        type: Boolean
    },
    categories: [
        {
            categoryName: {
                type: String,
                required: [true, 'Category Name is required']
            },
            isExpense: {
                type: Boolean,
                default: true
            }
        }
    ],
    expensesRecords: [
        {
            expenseName: {
                type: String,
                required: [true, 'Expense name required']
            },
            expenseAmount: {
                type: Number,
                required: [true, 'Amount Required']
            },
            expenseDate: {
                type: Date,
                default: moment()
            },
            isExpense: {
                type: Boolean,
                default: true
            }
        }
    ],
    incomeRecords: [
        {
            incomeName: {
                type: String,
                required:[true, 'Income name required']
            },
            incomeAmount: {
                type: Number,
                required: [true, 'Amount required']
            },
            incomeDate: {
                type: Date,
                default: moment()
            },
            isExpense: {
                type: Boolean,
                default: false
            }
        }
    ],
    travels: [
        {
            origin: {
                longitude: {
                    type: Number,
                    required: [true, 'Origin longitude is required']
                },
                latitude: {
                    type: Number,
                    required: [true, 'Origin latitude is required']
                }
            },
            destination: {
                longitude: {
                    type: Number,
                    required: [true, 'Destination longitude is required']
                },
                latitude: {
                    type: Number,
                    required: [true, 'Destination latitude is required']
                }
            },
            distance: {
                type: Number,
                required: [true, 'Distance is required.']
            },
            duration: {
                type: String,
                required: [true, 'Comment is required']
            },
            date: {
                type: Date,
                default: moment()
            },
             charge: {
                chargeId: {
                    type: String,
                    required: [true, 'Charge ID is required']
                },
                amount: {
                    type: Number,
                    required: [true, 'Charge amount is required']
                }
            } 
        }
    ]
})

module.exports = mongoose.model('user', userSchema)