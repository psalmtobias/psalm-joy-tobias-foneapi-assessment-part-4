import { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Link from 'next/link'

import UserContext from '../UserContext'

export default function NavBar() {
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">Mini Chat App</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    


                    {(user.id !== null)
                    ?
                        (user.accessType === "admin")
                        ? 
                            <React.Fragment>
                                <Link href="/recordsb">
                                    <a className="nav-link" role="button">Chat Here</a>
                                </Link>
                                <Link href="/userProfile">
                                    <a className="nav-link" role="button">Profile</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </React.Fragment>
                        :
                            (user.accessType === "staff")
                            ?
                                <React.Fragment>
                                    <Link href="/recordsb">
                                        <a className="nav-link" role="button">Chat Here</a>
                                    </Link>
                                    <Link href="/userProfile">
                                        <a className="nav-link" role="button">Profile</a>
                                    </Link>
                                    <Link href="/logout">
                                        <a className="nav-link" role="button">Logout</a>
                                    </Link>
                                </React.Fragment>
                            :
                                (user.accessType === "customer")
                                ?
                                    <React.Fragment>
                                        <Link href="/recordsb">
                                            <a className="nav-link" role="button">Chat Here</a>
                                        </Link>
                                        <Link href="/userProfile">
                                            <a className="nav-link" role="button">Profile</a>
                                        </Link>
                                        <Link href="/logout">
                                            <a className="nav-link" role="button">Logout</a>
                                        </Link>
                                    </React.Fragment>
                                :
                            <React.Fragment>
                                <Link href="/recordsb">
                                    <a className="nav-link" role="button">Chat Here</a>
                                </Link>
                                <Link href="/userProfile">
                                    <a className="nav-link" role="button">Profile</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link mx-2" role="button">Logout</a>
                                </Link>
                            </React.Fragment>
                    :   
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link mx-2" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link mx-2" role="button">Register</a>
                            </Link>


                        </React.Fragment>
                    }

                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}


                            // <Link href="/travel">
                            //     <a className="nav-link" role="button">Travel</a>
                            // </Link>
                            // <Link href="/insights">
                            //     <a className="nav-link" role="button">Insights</a>
                            // </Link>
                            // <Link href="/history">
                            //     <a className="nav-link" role="button">History</a>
                            // </Link>
