import { useState, useContext, useEffect } from 'react'
import { Form, Container, Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import Head from 'next/head';
import Router from 'next/router';


export default function userProfile({ data }) {
	const { user } = useContext(UserContext)

	const [ firstName, setFirstName ] = useState('')
    const [ lastName, setLastName ] = useState('')
    const [ mobileNo, setMobileNo ] = useState('')

    const [ showInfo, setShowInfo ] = useState(true)

	useEffect(() => {
		setFirstName(data[0].firstName)
		setLastName(data[0].lastName)
		setMobileNo(data[0].mobileNo)

		if(showInfo == true) {
			setShowInfo(false)
		} else {
			setShowInfo(true)
		}
	}, [])

	function updateUser(e) {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/updateUser`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				userId: user.id,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data ===true) {
				Router.push('/userProfile')
				Swal.fire(
                    'Updated Successfully!',
                    'You edited your profile!',
                    'success'
                )
			} else {
				Router.push('/userProfile')
				Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                })
			}
		})
	}


	console.log(data)

	return (
		<React.Fragment>
			<h1>User Profile</h1>
			<Container>
				<p>First Name: {firstName}</p>
				<p>Last Name: {lastName}</p>
				<p>MobileNo: {mobileNo}</p>
			</Container>
			<Button className="bg-warning">Show info</Button>
			<Form onSubmit={(e) => updateUser(e)}>
				<Container>
		            <Form.Group controlId="Service Name b">
		                <Form.Label>First Name:</Form.Label>
		                <Form.Control 
		                    type="text" 
		                    placeholder="Enter new name" 
		                    value={firstName} 
		                    onChange={e => {setFirstName(e.target.value)}}
		                    required
		                />
		            </Form.Group>
		            <Form.Group controlId="Service Description b">
		                <Form.Label>Last Name:</Form.Label>
		                <Form.Control 
		                    type="text" 
		                    placeholder="Enter new description"
		                    as="textarea"
		                    value={lastName} 
		                    onChange={e => {setLastName(e.target.value)}}
		                    required
		                />
		            </Form.Group>
		            <Form.Group controlId="Service Amount b">
		                <Form.Label>Mobile No.:</Form.Label>
		                <Form.Control 
		                    type="text" 
		                    placeholder="Enter new name" 
		                    value={mobileNo} 
		                    onChange={e => {setMobileNo(e.target.value)}}
		                    required
		                />
		            </Form.Group>
				</Container>
				<Button className="bg-primary" type="submit" block>Submit</Button>
			</Form>
		</React.Fragment>
	)
}

export async function getServerSideProps() {
	//fetch data from endpoint
	const res = await fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/getAllUsers`)
	const data = await res.json()

	//return the props
	return {
		props: {
			data
		}
	}
}