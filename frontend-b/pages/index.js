import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
	const data = {
		title: "Mini Chat App",
		content: "Talk now."
	}
	
	return (
		<React.Fragment>
			<Head >
				<title>Mini Chat App</title>
			</Head>
			<Banner data={data} />
		</React.Fragment>
	)
}