import Head from 'next/head'
import Banner from '../components/Banner'

// export default function error() {
//   return (
//     <React.Fragment>
//       <Head>
//         <title>Oops...</title>
//       </Head>
//       <Banner title="Something went wrong" content="Please try again later." />
//     </React.Fragment>
//   )
// }

export default function Home() {
	const data = {
		title: "Something went wrong",
		content: "Please try again later."
	}
	
	return (
		<React.Fragment>
			<Head>
				<title>Ooops</title>
			</Head>
			<Banner data={data} />
		</React.Fragment>
	)
}